#include "stingr.h"

UART_HandleTypeDef STINGR_HUART;

uint16_t crc16_lsb(uint8_t pData[], int length) {
	
	uint16_t crc;

	if (length == 0) return 0;
	
	crc = 0xFFFF;
	
	for(int i = 0; i < length; i++) {
		
		crc = crc ^ pData[i];
		
		for(int j = 0; j < 8; j++) {
			if (crc & 0x0001) crc = (crc >> 1) ^ 0x8408;
			else crc >>=1;
		}
		
	}
	
	crc = (uint16_t)~crc;
	
	return crc;
}

void stingr_SendPacket(uint8_t data[], int length, uint8_t command) {
	
	uint8_t transmitBuffer[149];
	
	transmitBuffer[0] = 0xAA;
	transmitBuffer[1] = length + 5;
	transmitBuffer[2] = command;

	for(int i = 0; i < length; i++) transmitBuffer[i + 3] = data[i];
	
	uint16_t crc = crc16_lsb(transmitBuffer, length + 3);
	
	transmitBuffer[length + 3] = crc&0xFF;
	transmitBuffer[length + 4] = crc>>8;

	HAL_UART_Transmit(&STINGR_HUART, transmitBuffer, length + 5, 1000);	
}


int stingr_ReceivePacket(uint8_t data[], int length) {
	
	uint8_t receiveBuffer[149];
	
	length += 5;
	
	HAL_UART_Receive(&STINGR_HUART, receiveBuffer, length, 1000);
	
	uint16_t crc = crc16_lsb(receiveBuffer, length - 2);
	
	if ((receiveBuffer[0] == 0xAA) &&
			(receiveBuffer[length - 1] == (crc>>8)) && 
			(receiveBuffer[length - 2] == (crc&0xFF))) {
		
		for(int i = 0; i < length - 5; i++) data[i] = receiveBuffer[i + 3];
		
		return 0;
	}
			
	return -1;
}


int stingr_SendData(uint8_t payload[], int length) {
	
	uint8_t receiveBuffer[5];
	
	stingr_SendPacket(payload, length, 0);
	
	return stingr_ReceivePacket(NULL, 0);
}


int stingr_QueryESN(uint8_t* receiveBuffer) {
	
	stingr_SendPacket(NULL, 0, 1);
	
	return stingr_ReceivePacket(receiveBuffer, 4);
}

int stingr_QuerySetup(uint8_t* receiveBuffer) {
	
	stingr_SendPacket(NULL, 0, 0x7);
	
	return stingr_ReceivePacket(receiveBuffer, 9);
}

int stingr_QueryStatus(uint8_t* receiveBuffer) {
	
	stingr_SendPacket(NULL, 0, 0x13);
	
	return stingr_ReceivePacket(receiveBuffer, 3);
}

int stingr_QueryLocation(uint8_t* receiveBuffer) {
	
	stingr_SendPacket(NULL, 0, 0x25);
	
	return stingr_ReceivePacket(receiveBuffer, 7);
}

int stingr_QueryBurst() {
	
	uint8_t receiveBuffer[1];
	
	stingr_SendPacket(NULL, 0, 0x4);

	int result;
	
	if (stingr_ReceivePacket(receiveBuffer, 1) == -1) result = -1;
	else result = receiveBuffer[0];

	return result;
}

int stingr_QueryTemperature() {
	
	int8_t data[] = { 0x17 };
	
	uint8_t receiveBuffer[2];
	
	stingr_SendPacket(data, 1, 0xFD);
	
	int result;
	
	if (stingr_ReceivePacket(receiveBuffer, 2) == -1) result = -1;
	else result = receiveBuffer[0] + ((int16_t)receiveBuffer[1] << 8);
	
	return result;
}

int stingr_AbortSend() {
	
	uint8_t receiveBuffer[5];

	stingr_SendPacket(NULL, 0, 0x3);
	
	HAL_UART_Receive(&STINGR_HUART, receiveBuffer, 5, 1000);
	
	return 0;
}

int stingr_InitTrack(uint8_t userData[3], int interval) {
	
	uint8_t receiveBuffer[5];
	
	uint8_t data[5];
	
	data[0] = interval >> 8;

	data[1] = interval & 0xFF;
	
	data[2] = userData[0];
	data[3] = userData[1];
	data[4] = userData[2];
	
	stingr_SendPacket(data, 5, 0x30);
	
	HAL_UART_Receive(&STINGR_HUART, receiveBuffer, 5, 1000);
	
	return 0;
}

int stingr_InitMotionTrack(uint8_t userData[3], int interval) {
	
	uint8_t receiveBuffer[5];
	
	uint8_t data[5];
	
	data[0] = interval >> 8;

	data[1] = interval & 0xFF;
	
	data[2] = userData[0];
	data[3] = userData[1];
	data[4] = userData[2];
	
	stingr_SendPacket(data, 5, 0x34);
	
	HAL_UART_Receive(&STINGR_HUART, receiveBuffer, 5, 1000);
	
	return 0;
}

int stingr_UpdateTrackData(uint8_t userData[3]) {
	
	uint8_t receiveBuffer[5];
	
	stingr_SendPacket(userData, 3, 0x31);
	
	HAL_UART_Receive(&STINGR_HUART, receiveBuffer, 5, 1000);
	
	return 0;
}

int stingr_CancelTrack() {
	
	uint8_t receiveBuffer[5];
	
	stingr_SendPacket(NULL, 0, 0x32);
	
	HAL_UART_Receive(&STINGR_HUART, receiveBuffer, 5, 1000);
	
	return 0;
}