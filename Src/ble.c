#include "ble.h"

UART_HandleTypeDef BLE_HUART;

int ble_send_receive(char *data, char *result) 
{
	int len = 0;
	while(!(data[len++] == '\r' && data[len] == '\n'));
	
	HAL_UART_Transmit(&BLE_HUART, data, len + 1, 1000);	
	
	HAL_UART_Receive(&BLE_HUART, result, 100, 1000);	
	/*
	for(int i = 0; i < 64; i++) {
		
		HAL_UART_Receive(&BLE_HUART, result + i, 1, 1000);	
		
		if (i >= 1 && result[i-1] == '\r' && result[i] == '\n') return 1;
	}*/
	
	return -1;
}