#ifndef __SETTINGS_H
#define __SETTINGS_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

#define EEPROM_START_ADDRESS 0x08080000
	
#define SETTINGS_MAGIC 0x1682	
	
struct EEPROM_Settings {
	uint16_t magic;
	uint16_t setupTime;
	uint16_t sendPeriod;
	uint16_t verticalAlertPercent;
}; 

#ifdef __cplusplus
}
#endif

#endif /* __SETTINGS_H */