#include "ICM.h"

struct TICM20948 ICM20948;



void delay_us ( uint32_t us )
{
volatile uint32_t delay = (us * (SystemCoreClock / 1000000) / 4);
while (delay--);
}


int ICM20948_init(I2C_HandleTypeDef bus, uint8_t address) {

	ICM20948.Bus = bus;
	ICM20948.Address = address;
	ICM20948._currentUserBank = -1;
	

  if (ICM20948_changeUserBank(USER_BANK_0) < 0) return -1;
	
  if (ICM20948_selectAutoClockSource() < 0) return -2;
	
  if (ICM20948_enableI2cMaster() < 0) return -3;
	
	if (ICM20948_powerDownMag() < 0) return -4;
	
  ICM20948_reset(); // reset the ICM20948. Don't check return value as a reset clears the register and can't be verified.
  delay_us(1000); // wait for ICM-20948 to come back up
  ICM20948_resetMag(); // Don't check return value as a reset clears the register and can't be verified.	
	
if (ICM20948_selectAutoClockSource() < 0) {
    return -6;
  }
  if (ICM20948_whoAmI() != ICM20948_WHO_AM_I) {
    return -7;
  }
  if (ICM20948_enableAccelGyro() < 0) {
    return -8;
  }
  if (ICM20948_configAccel(ACCEL_RANGE_16G, ACCEL_DLPF_BANDWIDTH_246HZ) < 0) {
    return -9;
  }
  if (ICM20948_configGyro(GYRO_RANGE_2000DPS, GYRO_DLPF_BANDWIDTH_197HZ) < 0) {
    return -10;
  }
  if (ICM20948_setGyroSrd(0) < 0) { 
    return -11;
  } 
  if (ICM20948_setAccelSrd(0) < 0) { 
    return -12;
  }
  if(ICM20948_enableI2cMaster() < 0) {
    return -13;
  }
	if(ICM20948_whoAmIMag() != MAG_AK09916_WHO_AM_I ) {
    return -14;
	}
  if(ICM20948_configMag() < 0){
    return -18;
  }
  if(ICM20948_selectAutoClockSource() < 0) { // TODO: Why do this again here?
    return -19;
  }       
	uint8_t _buffer[8];
	
  ICM20948_readMagRegisters(MAG_HXL, 8, _buffer); // instruct the ICM20948 to get data from the magnetometer at the sample rate
  return 1;	
	
}


int ICM20948_enableAccelGyro() {
	if (ICM20948_changeUserBank(USER_BANK_0) < 0 || ICM20948_writeRegister(UB0_PWR_MGMNT_2, UB0_PWR_MGMNT_2_SEN_ENABLE) < 0) {
    return -1;
  }
  return 1;
}



int ICM20948_selectAutoClockSource() {
	if (ICM20948_changeUserBank(USER_BANK_0) < 0 || ICM20948_writeRegister(UB0_PWR_MGMNT_1, UB0_PWR_MGMNT_1_CLOCK_SEL_AUTO) < 0) {
    return -1;
  }
  return 1;
}

int ICM20948_enableI2cMaster() {
	if (ICM20948_changeUserBank(USER_BANK_0) < 0) {
    return -1;
  }
  if (ICM20948_writeRegister(UB0_USER_CTRL, UB0_USER_CTRL_I2C_MST_EN) < 0) { // UB0_USER_CTRL_I2C_MST_EN 
    return -2;
  }
  if (ICM20948_changeUserBank(USER_BANK_3) < 0) {
    return -3;
  }
  if(ICM20948_writeRegister(UB3_I2C_MST_CTRL, UB3_I2C_MST_CTRL_CLK_400KHZ) < 0){
    return -4;
  }
  return 1;
}



int ICM20948_whoAmI() {
	
	uint8_t _buffer[1];
	
	if (ICM20948_changeUserBank(USER_BANK_0) < 0) {
  	return -1;
  }
  // read the WHO AM I register
  if (ICM20948_readRegisters(UB0_WHO_AM_I, 1, _buffer) < 0) {
    return -1;
  }
  // return the register value
  return _buffer[0];
}


int ICM20948_reset() {
	if (ICM20948_changeUserBank(USER_BANK_0) < 0) {
    return -1;
  }
  if (ICM20948_writeRegister(UB0_PWR_MGMNT_1, UB0_PWR_MGMNT_1_DEV_RESET) < 0) {
  	return -2;
  }
  return 1;
}


int ICM20948_configAccel(enum AccelRange range, enum AccelDlpfBandwidth bandwidth) {
	if (ICM20948_changeUserBank(USER_BANK_2) < 0) {
  	return -1;
  }
  uint8_t accelRangeRegValue = 0x00;
  float accelScale = 0.0f;
  switch(range) {
    case ACCEL_RANGE_2G: {
      accelRangeRegValue = UB2_ACCEL_CONFIG_FS_SEL_2G;
      accelScale = GravityConst * 2.0f/accRawScaling; // setting the accel scale to 2G
      break; 
    }
    case ACCEL_RANGE_4G: {
      accelRangeRegValue = UB2_ACCEL_CONFIG_FS_SEL_4G;
      accelScale = GravityConst * 4.0f/accRawScaling; // setting the accel scale to 4G
      break;
    }
    case ACCEL_RANGE_8G: {
      accelRangeRegValue = UB2_ACCEL_CONFIG_FS_SEL_8G;
      accelScale = GravityConst * 8.0f/accRawScaling; // setting the accel scale to 8G
      break;
    }
    case ACCEL_RANGE_16G: {
      accelRangeRegValue = UB2_ACCEL_CONFIG_FS_SEL_16G;
      accelScale = GravityConst * 16.0f/accRawScaling; // setting the accel scale to 16G
      break;
    }
  }
  uint8_t dlpfRegValue = 0x00;
  switch(bandwidth) {
  	case ACCEL_DLPF_BANDWIDTH_1209HZ: dlpfRegValue = UB2_ACCEL_CONFIG_DLPFCFG_1209HZ; break;
  	case ACCEL_DLPF_BANDWIDTH_246HZ: dlpfRegValue = UB2_ACCEL_CONFIG_DLPFCFG_246HZ; break;
  	case ACCEL_DLPF_BANDWIDTH_111HZ: dlpfRegValue = UB2_ACCEL_CONFIG_DLPFCFG_111HZ; break;
  	case ACCEL_DLPF_BANDWIDTH_50HZ: dlpfRegValue = UB2_ACCEL_CONFIG_DLPFCFG_50HZ; break;
  	case ACCEL_DLPF_BANDWIDTH_24HZ: dlpfRegValue = UB2_ACCEL_CONFIG_DLPFCFG_24HZ; break;
  	case ACCEL_DLPF_BANDWIDTH_12HZ: dlpfRegValue = UB2_ACCEL_CONFIG_DLPFCFG_12HZ; break;
  	case ACCEL_DLPF_BANDWIDTH_6HZ: dlpfRegValue = UB2_ACCEL_CONFIG_DLPFCFG_6HZ; break;
  	case ACCEL_DLPF_BANDWIDTH_473HZ: dlpfRegValue = UB2_ACCEL_CONFIG_DLPFCFG_473HZ; break;
  }
  if (ICM20948_writeRegister(UB2_ACCEL_CONFIG, accelRangeRegValue | dlpfRegValue) < 0) {
		return -1;
  }
  ICM20948._accelScale = accelScale;
  ICM20948._accelRange = range;
  ICM20948._accelBandwidth = bandwidth;
  return 1;
}

/* sets the gyro full scale range to values other than default */
int ICM20948_configGyro(enum GyroRange range, enum GyroDlpfBandwidth bandwidth) {
  if (ICM20948_changeUserBank(USER_BANK_2) < 0) {
  	return -1;
  }
  uint8_t gyroConfigRegValue = 0x00;
  float gyroScale = 0x00;
  switch(range) {
    case GYRO_RANGE_250DPS: {
    	gyroConfigRegValue = UB2_GYRO_CONFIG_1_FS_SEL_250DPS;
      gyroScale = 250.0f/gyroRawScaling * Pi /180.0f; // setting the gyro scale to 250DPS
      break;
    }
    case GYRO_RANGE_500DPS: {
      gyroConfigRegValue = UB2_GYRO_CONFIG_1_FS_SEL_500DPS;
      gyroScale = 500.0f/gyroRawScaling * Pi /180.0f; // setting the gyro scale to 500DPS
      break;  
    }
    case GYRO_RANGE_1000DPS: {
      gyroConfigRegValue = UB2_GYRO_CONFIG_1_FS_SEL_1000DPS;
      gyroScale = 1000.0f/gyroRawScaling * Pi /180.0f; // setting the gyro scale to 1000DPS
      break;
    }
    case GYRO_RANGE_2000DPS: {
      gyroConfigRegValue = UB2_GYRO_CONFIG_1_FS_SEL_2000DPS;
      gyroScale = 2000.0f/gyroRawScaling * Pi /180.0f; // setting the gyro scale to 2000DPS
      break;
    }
  }
  uint8_t dlpfRegValue = 0x00;
  switch(bandwidth) {
  	case GYRO_DLPF_BANDWIDTH_12106HZ: dlpfRegValue = UB2_GYRO_CONFIG_1_DLPFCFG_12106HZ; break;
  	case GYRO_DLPF_BANDWIDTH_197HZ: dlpfRegValue = UB2_GYRO_CONFIG_1_DLPFCFG_197HZ; break;
  	case GYRO_DLPF_BANDWIDTH_152HZ: dlpfRegValue = UB2_GYRO_CONFIG_1_DLPFCFG_152HZ; break;
  	case GYRO_DLPF_BANDWIDTH_120HZ: dlpfRegValue = UB2_GYRO_CONFIG_1_DLPFCFG_120HZ; break;
  	case GYRO_DLPF_BANDWIDTH_51HZ: dlpfRegValue = UB2_GYRO_CONFIG_1_DLPFCFG_51HZ; break;
  	case GYRO_DLPF_BANDWIDTH_24HZ: dlpfRegValue = UB2_GYRO_CONFIG_1_DLPFCFG_24HZ; break;
  	case GYRO_DLPF_BANDWIDTH_12HZ: dlpfRegValue = UB2_GYRO_CONFIG_1_DLPFCFG_12HZ; break;
  	case GYRO_DLPF_BANDWIDTH_6HZ: dlpfRegValue = UB2_GYRO_CONFIG_1_DLPFCFG_6HZ; break;
  	case GYRO_DLPF_BANDWIDTH_361HZ: dlpfRegValue = UB2_GYRO_CONFIG_1_DLPFCFG_361HZ; break;
  }
  if (ICM20948_writeRegister(UB2_GYRO_CONFIG_1, gyroConfigRegValue | dlpfRegValue) < 0) {
  	return -1;
	}
	ICM20948._gyroScale = gyroScale;
  ICM20948._gyroRange = range;
  ICM20948._gyroBandwidth = bandwidth;
  return 1;
}


int ICM20948_setGyroSrd(uint8_t srd) {
	if (ICM20948_changeUserBank(USER_BANK_2) < 0 || ICM20948_writeRegister(UB2_GYRO_SMPLRT_DIV, srd) < 0) {
  	return -1;
	}
	ICM20948._gyroSrd = srd;
	return 1;
}

int ICM20948_setAccelSrd(uint16_t srd) {
	if (ICM20948_changeUserBank(USER_BANK_2) < 0) {
  	return -1;
  }
  uint8_t srdHigh = srd >> 8 & 0x0F; // Only last 4 bits can be set
  if (ICM20948_writeRegister(UB2_ACCEL_SMPLRT_DIV_1, srdHigh) < 0) {
  	return -1;
	}
	uint8_t srdLow = srd & 0x0F; // Only last 4 bits can be set
  if (ICM20948_writeRegister(UB2_ACCEL_SMPLRT_DIV_2, srdLow) < 0) {
  	return -1;
	}
	ICM20948._accelSrd = srd;
	return 1;
}









int ICM20948_configMag() { // TODO: Add possibility to use other modes
	if (ICM20948_writeMagRegister(MAG_CNTL2, MAG_CNTL2_MODE_100HZ) < 0) {
		return -1;
	}
	return 1;
}

int ICM20948_resetMag() {
	if (ICM20948_writeMagRegister(MAG_CNTL3, MAG_CNTL3_RESET) < 0) {
		return -1;
	}
	return 1;
}

int ICM20948_changeUserBank(enum UserBank userBank) {

	uint8_t userBankRegValue = 0x00;
	
	switch(userBank) {
    case USER_BANK_0: {
    	userBankRegValue = REG_BANK_SEL_USER_BANK_0;
  		break;
    }
    case USER_BANK_1: {
    	userBankRegValue = REG_BANK_SEL_USER_BANK_1;
  		break;
    }
    case USER_BANK_2: {
    	userBankRegValue = REG_BANK_SEL_USER_BANK_2;
  		break;
    }
    case USER_BANK_3: {
    	userBankRegValue = REG_BANK_SEL_USER_BANK_3;
  		break;
    }
  }
  if (ICM20948_writeRegister(REG_BANK_SEL, userBankRegValue) < 0) {
  	return -1;
  }
	
  ICM20948._currentUserBank = userBank;
  return 1;
}


int ICM20948_powerDownMag() {
	
	if (ICM20948_writeMagRegister(MAG_CNTL2, MAG_CNTL2_POWER_DOWN) < 0) {
		return -1;
	}
	return 1;
}

int ICM20948_whoAmIMag() {
	
	uint8_t _buffer[2];
	
	if (ICM20948_readMagRegisters(MAG_WHO_AM_I, 2, _buffer) < 0) {
    return -1;
  }
	
  return (_buffer[0] << 8) + _buffer[1];
}


int ICM20948_writeMagRegister(uint8_t subAddress, uint8_t data) {
	if (ICM20948_changeUserBank(USER_BANK_3) < 0) {
  	return -1;
  }
	if (ICM20948_writeRegister(UB3_I2C_SLV0_ADDR, MAG_AK09916_I2C_ADDR) < 0) {
    return -2;
  }
  // set the register to the desired magnetometer sub address 
	if (ICM20948_writeRegister(UB3_I2C_SLV0_REG, subAddress) < 0) {
    return -3;
  }
  // store the data for write
	if (ICM20948_writeRegister(UB3_I2C_SLV0_DO, data) < 0) {
    return -4;
  }
  // enable I2C and send 1 byte
	if (ICM20948_writeRegister(UB3_I2C_SLV0_CTRL, UB3_I2C_SLV0_CTRL_EN | (uint8_t)1) < 0) {
    return -5;
  }
	
	uint8_t result;
	
	// read the register and confirm
	if (ICM20948_readMagRegisters(subAddress, 1, &result) < 0) {
    return -6;
  }
	if(result != data) {
  	return -7;
  }
  return 1;
}

int ICM20948_readMagRegisters(uint8_t subAddress, uint8_t count, uint8_t* dest) {
	if (ICM20948_changeUserBank(USER_BANK_3) < 0) {
  	return -1;
  }
	if (ICM20948_writeRegister(UB3_I2C_SLV0_ADDR, MAG_AK09916_I2C_ADDR | UB3_I2C_SLV0_ADDR_READ_FLAG) < 0) {
    return -2;
  }
  // set the register to the desired magnetometer sub address
	if (ICM20948_writeRegister(UB3_I2C_SLV0_REG, subAddress) < 0) {
    return -3;
  }
  // enable I2C and request the bytes
	if (ICM20948_writeRegister(UB3_I2C_SLV0_CTRL, UB3_I2C_SLV0_CTRL_EN | count) < 0) {
    return -4;
  }
	delay_us(1000); // takes some time for these registers to fill
  // read the bytes off the ICM-20948 EXT_SLV_SENS_DATA registers
  if (ICM20948_changeUserBank(USER_BANK_0) < 0) {
  	return -5;
  }
	uint8_t _status = ICM20948_readRegisters(UB0_EXT_SLV_SENS_DATA_00, count, dest); 
  return _status;
}

void ICM20948_populate_data(uint8_t *data) {
	for(int i = 0; i < 20; i++) data[i] = ICM20948.RawData[i];
}

int ICM20948_readSensor() {
	
  if (ICM20948_changeUserBank(USER_BANK_0) < 0) return -1;	

  if (ICM20948_readRegisters(UB0_ACCEL_XOUT_H, 20, ICM20948.RawData) < 0) return -2;	
	
	int16_t 	ax = (int16_t)ICM20948.RawData[0] << 8 | ICM20948.RawData[1],
						ay = (int16_t)ICM20948.RawData[2] << 8 | ICM20948.RawData[3],
						az = (int16_t)ICM20948.RawData[4] << 8 | ICM20948.RawData[5];
	
	int16_t 	gx = (int16_t)ICM20948.RawData[6] << 8 | ICM20948.RawData[7],
						gy = (int16_t)ICM20948.RawData[8] << 8 | ICM20948.RawData[9],
						gz = (int16_t)ICM20948.RawData[10] << 8 | ICM20948.RawData[11];

	int16_t 	t = (int16_t)ICM20948.RawData[12] << 8 | ICM20948.RawData[13];
	
	int16_t 	mx = (int16_t)ICM20948.RawData[15] << 8 | ICM20948.RawData[14],
						my = (int16_t)ICM20948.RawData[17] << 8 | ICM20948.RawData[16],
						mz = (int16_t)ICM20948.RawData[19] << 8 | ICM20948.RawData[18];
	
	ICM20948.Accel.x = (float)ax * ICM20948._accelScale;
	ICM20948.Accel.y = (float)ay * ICM20948._accelScale;
	ICM20948.Accel.z = (float)az * ICM20948._accelScale;
	
	ICM20948.Gyro.x = (float)gx * ICM20948._gyroScale;
	ICM20948.Gyro.y = (float)gy * ICM20948._gyroScale;
	ICM20948.Gyro.z = (float)gz * ICM20948._gyroScale;
	
	ICM20948.Temperature = (float)(t - _tempOffset) / _tempScale + _tempOffset;
	
	ICM20948.Mag.x = (float)mx * _magScale;
	ICM20948.Mag.y = (float)my * _magScale;
	ICM20948.Mag.z = (float)mz * _magScale;
	
	
	return 1;
}

int8_t ICM20948_checkVertical(int8_t verticalAlertPercent) {
	
	ICM20948_readSensor();
		
	float verticalPercent = ICM20948.Accel.z * ICM20948.Accel.z / (ICM20948.Accel.z * ICM20948.Accel.z + ICM20948.Accel.x * ICM20948.Accel.x + ICM20948.Accel.y * ICM20948.Accel.y);
		
	return (verticalPercent > (float)(verticalAlertPercent * verticalAlertPercent) / 10000) && (ICM20948.Accel.z < 0) ? 1 : 0;
}

void ICM20948_GetAccelAngle(float *result) {
	

}

int ICM20948_readRegisters(int8_t reg, uint8_t length, uint8_t *res) {
	
	if (HAL_I2C_Master_Transmit(&ICM20948.Bus, (ICM20948.Address << 1), (uint8_t []){reg}, 1, 0xFFFF) != HAL_OK) return -1;	
	
	if (HAL_I2C_Master_Receive(&ICM20948.Bus, (ICM20948.Address << 1), res, length, 0xFFFF) != HAL_OK) return -2;	
	
	return 1;
}

int ICM20948_writeRegister(int8_t reg, uint8_t data) {
	
	if (HAL_I2C_Master_Transmit(&ICM20948.Bus, (ICM20948.Address << 1), (uint8_t []){reg, data}, 2, 0xFFFF) != HAL_OK) return -1;	
	
	delay_us(10);
	
	uint8_t result;
	
	ICM20948_readRegisters(reg, 1, &result);
	
	if (result != data) return -1;
	
	return 1;
}
