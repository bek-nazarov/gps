#include "settings.h"

struct EEPROM_Settings DefaultSetting = {SETTINGS_MAGIC, 10 * 60, 15 * 60, 95};

struct EEPROM_Settings CurrentSettings;



void writeToEEPROM (uint32_t address, uint32_t value)
{
  HAL_StatusTypeDef flash_ok = HAL_ERROR;
  while (flash_ok != HAL_OK)
  {
    flash_ok = HAL_FLASHEx_DATAEEPROM_Unlock();
  }
  flash_ok = HAL_ERROR;
  while (flash_ok != HAL_OK)
  {
    flash_ok = HAL_FLASHEx_DATAEEPROM_Erase (address);
  }
  flash_ok = HAL_ERROR;
  while (flash_ok != HAL_OK)
  {
    flash_ok = HAL_FLASHEx_DATAEEPROM_Program (FLASH_TYPEPROGRAMDATA_WORD, address, value);
  }
  flash_ok = HAL_ERROR;
  while (flash_ok != HAL_OK)
  {
    flash_ok = HAL_FLASHEx_DATAEEPROM_Lock ();
  }
}

uint8_t* ParseValue(uint8_t *buffer, uint8_t *value) {
	int i = 0, vs = 0;
	while(buffer[i] == ',' || buffer[i] == ' ' || buffer[i] == '\r') i++;	vs = i;
	while(buffer[i] != ',' && buffer[i] != ' ' && buffer[i] != '\r') i++;
	strncpy(value, buffer + vs, i - vs);
	value[i - vs] = 0;
	return buffer + i;
}

void ParseSettings(uint8_t *buffer) {

	int i = 0;

	uint8_t value[6];
	
	buffer = ParseValue(buffer, value);	CurrentSettings.setupTime = atoi(value);
	buffer = ParseValue(buffer, value);	CurrentSettings.sendPeriod = atoi(value);
	buffer = ParseValue(buffer, value);	CurrentSettings.verticalAlertPercent = atoi(value);
}

void LoadSettings() {

	memcpy(&CurrentSettings, EEPROM_START_ADDRESS, sizeof(CurrentSettings));
	
	if (CurrentSettings.magic != SETTINGS_MAGIC) {
		memcpy(&CurrentSettings, &DefaultSetting, sizeof(CurrentSettings));
	}
}

void SaveSettings() {

	for(int i = 0; i < sizeof(CurrentSettings) / 4; i++) {
		writeToEEPROM(EEPROM_START_ADDRESS + i * 4, ((uint32_t*)&CurrentSettings)[i]);
	}

}